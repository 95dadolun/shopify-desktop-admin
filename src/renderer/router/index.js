import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: require('@/components/LandingPage').default
    },
    {
      path: '/product/list',
      name: 'products',
      component: require('@/components/ProductList').default
    },
    {
      path: '/product/view/:sku',
      name: 'product',
      component: require('@/components/ProductView').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
